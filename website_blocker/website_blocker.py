import sys, os, time, json
from sys import platform
from datetime import datetime as dt

if (platform == "linux" or platform == "linux2") or platform == "darwin" :
    host_path = "/etc/hosts"
elif platform == "win32" :
    host_path = r"C:\Windows\System32\drivers\etc\hosts" #r - means passing a raw string
else :
    print("Unsupported OS")
    os.system("pause")
    sys.exit()

redirect = "127.0.0.1"

try :
    with open("config.json") as config_file:
        json_data = json.load(config_file)
        website_list = json_data["website_list"]
        working_hours_start = int(json_data["working_hours"]["start"])
        working_hours_end = int(json_data["working_hours"]["end"])
except :
    print("cannot load config file or wrong config data")
    os.system("pause")
    sys.exit()

while True:
    if dt(dt.now().year,dt.now().month,dt.now().day, working_hours_start) < dt.now() < dt(dt.now().year,dt.now().month,dt.now().day, working_hours_end) :
        print("Working hours...")
        with open(host_path,'r+') as file:
            content = file.read()
            for website in website_list:
                if website in content:
                    pass
                else:
                    file.write(redirect + " " +  website + "\n")
    else:
        with open(host_path,'r+') as file:
            content = file.readlines()
            file.seek(0)
            for line in content:
                if not any(website in line for website in website_list):
                    file.write(line)
            file.truncate()
        print("Fun hours...")
    time.sleep(5)

os.system("pause")
