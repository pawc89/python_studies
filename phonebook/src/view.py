from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import csv
from src import controller


class UIWindow(object):

	def __init__( self, window = Tk() ):
		self.window = window
		self.main_controller = controller.MainController()
		self.create_ui()
		self.list_ids = []
		self.selected_list_item = None
		self.window.mainloop()


	def create_ui(self):

		self.window.wm_title("PhoneBook")


		name_label = Label(self.window, text="First Name")
		self.name_text = StringVar()
		self.name_entry = Entry(self.window, width=40, textvariable=self.name_text)

		surname_label = Label(self.window, text="Last Name")
		self.surname_text = StringVar()
		self.surname_entry = Entry(self.window, width=40, textvariable=self.surname_text)

		phone_label = Label(self.window, text="Phone Number")
		self.phone_text = StringVar()
		self.phone_entry = Entry(self.window, width=40, textvariable=self.phone_text)

		info_label = Label(self.window, text="Additonal Info")
		self.info_text = StringVar()
		self.info_entry = Entry(self.window, width=40, textvariable=self.info_text)

		self.data_list = Listbox(self.window, height=9, width=55)
		data_list_scrollbar = Scrollbar(self.window)
		self.data_list.configure(yscrollcommand=data_list_scrollbar.set)
		data_list_scrollbar.configure(command=self.data_list.yview)
		self.data_list.bind('<<ListboxSelect>>', self.get_selected_row)

		view_btn = Button(self.window, text="View all", width=33, command=self.view_command)
		search_btn = Button(self.window, text="Search entry", width=33, command=self.search_command)
		add_btn = Button(self.window, text="Add entry", width=33, command=self.add_command)
		update_btn = Button(self.window, text="Update selected", width=33, command=self.update_command)
		delete_btn = Button(self.window, text="Delete selected", width=33, command=self.delete_command)
		export_btn = Button(self.window, text="Export into CSV", width=33, command=self.export_command)


		name_label.grid			(row=0, column=0)
		self.name_entry.grid	(row=0, column=1)
		surname_label.grid		(row=0, column=2)
		self.surname_entry.grid	(row=0, column=3)
		phone_label.grid		(row=1, column=0)
		self.phone_entry.grid	(row=1, column=1)
		info_label.grid			(row=1, column=2)
		self.info_entry.grid	(row=1, column=3)
		self.data_list.grid		(row=2, column=0, rowspan=6, columnspan=2)
		data_list_scrollbar.grid(row=2, column=2, rowspan=6)
		view_btn.grid			(row=2, column=3)
		search_btn.grid			(row=3, column=3)
		add_btn.grid			(row=4, column=3)
		update_btn.grid			(row=5, column=3)
		delete_btn.grid			(row=6, column=3)
		export_btn.grid			(row=7, column=3)




	def get_selected_row(self, event):
		if len(self.data_list.curselection()) > 0:
			index = self.data_list.curselection()[0]
			self.entries_clear()
			self.selected_list_item = self.list_ids[index]["id"]
			self.name_entry.insert(END, self.list_ids[index]["name"])
			self.surname_entry.insert(END,  self.list_ids[index]["surname"])
			self.phone_entry.insert(END,  self.list_ids[index]["phone"])
			self.info_entry.insert(END,  self.list_ids[index]["info"])


	def add_command(self) :
		add_result = self.main_controller.add_command( self.surname_text.get().strip(), self.phone_text.get().strip(), self.name_text.get().strip(), self.info_text.get().strip() )
		self.data_list.delete(0, END)
		self.list_ids = []
		self.selected_list_item = None
		messagebox.showinfo(add_result["result"][0], add_result["result"][1])
		if add_result["result"][0] == "success" :
			self.entries_clear()
			self.list_ids.append({
				"id": add_result["data"]["id"],
				"surname": add_result["data"]["surname"],
				"name": add_result["data"]["name"],
				"phone": add_result["data"]["phone"],
				"info": add_result["data"]["info"]
			})
			self.data_list.insert(END, (add_result["data"]["surname"]+(", "+add_result["data"]["name"] if not add_result["data"]["name"] ==''  else '')+": "+ add_result["data"]["phone"]+" "+add_result["data"]["info"]))


	def view_command(self):
		view_results = self.main_controller.view_search_command()
		self.data_list.delete(0, END)
		self.list_ids = []
		self.selected_list_item = None
		if len(view_results["result"]) == 2 and len(view_results["data"]) == 0:
			messagebox.showinfo(view_results["result"][0], view_results["result"][1])
		else:
			i = 1
			for view_result in view_results["data"]:
				self.list_ids.append({
					"id": view_result["id"],
					"surname": view_result["surname"],
					"name": view_result["fname"],
					"phone": view_result["phone"],
					"info": view_result["info"]
				})
				self.data_list.insert(END, (str(i)+". "+view_result["surname"]+(", "+view_result["fname"] if not view_result["fname"] ==''  else '')+": "+view_result["phone"]+" "+view_result["info"]))
				i += 1


	def search_command(self):
		search_results = self.main_controller.view_search_command({"surname":self.surname_text.get().strip(), "phone":self.phone_text.get().strip(), "fname":self.name_text.get().strip(), "info":self.info_text.get().strip()})
		self.data_list.delete(0, END)
		self.list_ids = []
		self.selected_list_item = None
		if not search_results["result"][0] == '' and not search_results["result"][1] == '':
			messagebox.showinfo(search_results["result"][0], search_results["result"][1])
		else:
			i = 1
			for search_result in search_results["data"]:
				self.list_ids.append({
					"id": search_result["id"],
					"surname": search_result["surname"],
					"name": search_result["fname"],
					"phone": search_result["phone"],
					"info": search_result["info"]
				})
				self.data_list.insert(END, (str(i)+". "+search_result["surname"]+(", "+search_result["fname"] if not search_result["fname"] ==''  else '')+": "+search_result["phone"]+" "+search_result["info"]))
				i += 1
				

	def update_command(self):
		if self.selected_list_item is not None:
			update_results = self.main_controller.update_command( int(self.selected_list_item), self.surname_text.get().strip(), self.phone_text.get().strip(), self.name_text.get().strip(), self.info_text.get().strip() )
			if update_results["result"][0] == "success" :
				self.data_list.delete(0, END)
				self.list_ids = []
				self.selected_list_item = None
				self.entries_clear()
				self.list_ids.append({
					"id": update_results["data"]["id"],
					"surname": update_results["data"]["surname"],
					"name": update_results["data"]["name"],
					"phone": update_results["data"]["phone"],
					"info": update_results["data"]["info"]
				})
				self.data_list.insert(END, (update_results["data"]["surname"]+(", "+update_results["data"]["name"] if not update_results["data"]["name"] ==''  else '')+": "+ update_results["data"]["phone"]+" "+update_results["data"]["info"]))
			else:
				messagebox.showinfo(update_results["result"][0], update_results["result"][1])
		else:
			self.entries_clear()
			messagebox.showinfo("", "Nothing is selected in the list")


	def delete_command(self):
		if self.selected_list_item is not None:
			result = messagebox.askquestion("Delete", "Are You Sure?", icon='warning')
			if result == 'yes':
				delete_results = self.main_controller.delete_command(int(self.selected_list_item))
				self.data_list.delete(0, END)
				self.list_ids = []
				self.selected_list_item = None
				messagebox.showinfo(delete_results["result"][0], delete_results["result"][1])
				if len(delete_results["data"]) > 0:
					i = 1
					for delete_result in delete_results["data"]:
						self.list_ids.append({
						"id": delete_result["id"],
						"surname": delete_result["surname"],
						"name": delete_result["fname"],
						"phone": delete_result["phone"],
						"info": delete_result["info"]
						})
						self.data_list.insert(END, (str(i)+". "+delete_result["surname"]+(", "+delete_result["fname"] if not delete_result["fname"] ==''  else '')+": "+delete_result["phone"]+" "+delete_result["info"]))
						i += 1		
		else:
			messagebox.showinfo("", "Nothing is selected in the list")


	def export_command(self):
		export_results = self.main_controller.export_command()
		self.file_save(export_results)
		
		
	def file_save(self, file_txt):
		f = filedialog.asksaveasfile(mode='w', initialfile="phonebook.csv")
		if f is None: # asksaveasfile return `None` if dialog closed with "cancel".
			return
		writer = csv.writer(f, delimiter=',', quoting=csv.QUOTE_ALL, lineterminator='\n')
		for line in file_txt :
			writer.writerow(line)			
		f.close()
		
		
	def entries_clear(self):	
		self.surname_entry.delete(0, END)
		self.phone_entry.delete(0, END)
		self.name_entry.delete(0, END)
		self.info_entry.delete(0, END)
		
