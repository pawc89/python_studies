from src import manager

class MainController :

	def __init__(self) :
		self.db_manager = manager.DBManager()


	def add_command( self, surname_text='', phone_text='', name_text='', info_text='' ) :
		if surname_text == '' or phone_text == '' :
			return {
				"result": ["error", "Last Name and Phone Number fields cannot be empty!"]
			}			
		db_insert_result = self.db_manager.insert(surname_text, phone_text, name_text, info_text)		
		if len(db_insert_result) > 0 :
			return {
				"result": ["success", "Added: " + db_insert_result[0]["surname"] + ": " + db_insert_result[0]["phone"]],
				"data": {
					"id": db_insert_result[0]["id"],
					"name": db_insert_result[0]["fname"],
					"surname": db_insert_result[0]["surname"],
					"phone": db_insert_result[0]["phone"],
					"info": db_insert_result[0]["info"]
				}
			}
		else :
			return {
				"result": ["error", "failed to add data"],
				"data": []
			}
			

	def view_search_command( self, search_args={} ) :
		if len(search_args) > 0:
			not_empty = False
			for search_arg in search_args:
				if not search_args[search_arg] == '':
					not_empty = True
					break
			if not_empty == False:
				return {
					"result": ["message", "Nothing to search. All fields are empty"],
					"data": []
				}
			
			
		db_view_result = self.db_manager.view_search_command(search_args)
		if len(db_view_result) > 0 :
			return {
				"result": ["success",""],
				"data": db_view_result
			}
		else :
			return {
				"result": ["message", "no data retrieved"],
				"data": []
			}
		

	def update_command( self, item_id=None, surname_text='', phone_text='', name_text='', info_text='' ) :
		if item_id is None :
			return {
				"result": ["error", "System error. Nothing updated"]
			}
			
		if surname_text == '' or phone_text == '' :
			return {
				"result": ["error", "Last Name and Phone Number fields cannot be empty!"]
			}
		db_update_result = self.db_manager.update(item_id, surname_text, phone_text, name_text, info_text)		
		if len(db_update_result) > 0 :
			return {
				"result": ["success", "Updated: " + db_update_result[0]["surname"] + ": " + db_update_result[0]["phone"]],
				"data": {
					"id": db_update_result[0]["id"],
					"name": db_update_result[0]["fname"],
					"surname": db_update_result[0]["surname"],
					"phone": db_update_result[0]["phone"],
					"info": db_update_result[0]["info"]
				}
			}
		else :
			return {
				"result": ["error", "failed to update data"],
				"data": []
			}
		

	def delete_command( self, item_id ) :
		db_delete_result = self.db_manager.delete(item_id)
		db_view_result = self.db_manager.view_search_command()
		if db_delete_result == 0:
			return {
				"result": ["error", "failed to delete"],
				"data": db_view_result
			}
		elif db_delete_result > 1:
			return {
				"result": ["error", "System error occured: more items deleted"],
				"data": db_view_result
			}
		elif db_delete_result == 1:
			return {
				"result": ["success", "Successfully deleted"],
				"data": db_view_result
			}
		

	def export_command(self):
		db_view_results = self.db_manager.view_search_command()
		data4csv  = []
		if len(db_view_results) > 0 :
			data4csv.append(["LAST NAME", "FIRST NAME", "PHONE", "INFO"])
			for db_view_result in db_view_results :
				data4csv_row = [db_view_result["surname"], db_view_result["fname"], db_view_result["phone"], db_view_result["info"]]
				data4csv.append(data4csv_row)
			return data4csv
		else:
			return []
