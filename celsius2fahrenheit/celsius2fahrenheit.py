import datetime
import os
import pandas
from sys import platform

def logging(log_data):
    log_file = open('celsius2fahrenheit_log.txt', 'a')
    log_file.write(log_data)
    log_file.close();

def celsius2fahrenheit():
    temp_scale = input("\nTo convert Celsius -> Fahrenheit, enter CF. \nTo convert Fahrenheit -> Celsius, enter FC : ").strip()
    temperature = float(input("The temperature value to convert: "))
    temperature_calculated = 0.0

    if temp_scale.upper() == "CF" :
        if temperature < -273.15 :
            return "ERROR: The temperature cannot be lower than -273.15 Celsius"
        temperature_calculated =  temperature * (9/5) + 32
        logging("{}\tC->F\t{}\t{}\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), temperature, temperature_calculated))
        return temperature_calculated
    elif temp_scale.upper() == "FC" :
        if temperature < -459.67 :
            return "ERROR: The temperature cannot be lower than -459.67 Fahrenheit"
        temperature_calculated = (temperature - 32) * (5/9)
        logging("{}\tF->C\t{}\t{}\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), temperature_calculated, temperature))
        return temperature_calculated
    else :
        return "ERROR: Wrong temperature scale"

def history_log():
    history_log = pandas.read_csv("celsius2fahrenheit_log.txt", sep="\t", names=["Date", "C->F/F->C", "Celsius", "Fahrenheit"])
    print(history_log)

print(celsius2fahrenheit())

print("\nConversion History:\n")
history_log()

if(platform != "linux") : os.system("pause")
